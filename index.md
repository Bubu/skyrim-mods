---
layout: page
title: Skyrim Re-Done
---

* Do not remove this line (it will not be displayed)
{:toc}

## New Areas & Expansions

### Moonpath to Elsweyr ([Nexus](https://www.nexusmods.com/skyrim/mods/9782))

Only the Khajiit know the secret paths of the moon that lead the followers of the Mane back to Elsweyr.

Now one of these paths is open to the dova of Skyrim, long thought lost this path leads to a base deep in the heart of the jungles of Elsweyr.

You can find the Moonpath inside Falkreath's Dead man's Drink inn.

![Moonpath to Elsweyr 1]({{ "assets/moonpath1.jpg" }})

![Moonpath to Elsweyr 2]({{ "assets/moonpath2.jpg" }})

### Falskaar ([Nexus](https://www.nexusmods.com/skyrim/mods/37994))

A DLC sized new lands mod that adds 20+ hours of fully voiced lore friendly (But not canon) quests, dungeons, and more. 
 
For hundreds of years Falskaar has been inhabited by the Nords, though most of its history from before then is unknown.

For the first time since the party's arrival in Falskaar almost 600 years ago a portal activates deep within the ruins of Mzubthand and the player steps through, arriving in Falskaar. They are denoted 'The Traveler', based on an old prophecy.

"They who arrive through the shimmering wall mark the start of the worst."

#### What is Falskaar?

Falskaar is a new lands mod that adds an entirely new worldspace to the world. It's accessed by a dungeon the first time, then by boat from then on out. Falskaar's goal was to sharpen my skills in almost all areas around the board, with a focus in level and dungeon design. The result is that there is at least a little bit of everything. There is a new land, places, people, quests, dungeons and more for the player to experience. It adds roughly 20+ hours of content, and favors all types of characters.

To start Falskaar, go to Riften and speak with Jalamar. He will either be outside around the market, inside the northern gate, or inside the Bee and Barb. You can also just go straight into Echo Deep Mine, a new dungeon just northeast of Riften.

This is **NOT** a beta. This is **NOT** a test version. This is a 100% completed new lands mod.
(New and/or updated content may still be added, but the main production is complete)

#### Features

* An entirely new land independent of Tamriel, roughly the size of 2-3 Skyrim holds.
* 20-30+ hours of gameplay.
* 26 quests, including a 9 quest long main story, and 17 side quests! (Along with some unmarked content)
* New items including new books, recipes, weapons and armors. (A mix of brand new, and retextured)*
* Two new spells and a new shout.*
* A bard with several unique new songs.
* A soundtrack containing 14 brand new tracks composed by Adamm Khuevrr just for Falskaar, adding more than 40 minutes of new music!
* A fully voiced experience, featuring almost 30 semi-professional and professional voice actors and actresses.

![Faalskar]({{ "assets/falskaar.png" }})

### [Wyrmstooth](http://tes-mods.wikia.com/wiki/Wyrmstooth)

![Wyrmstooth]({{ "assets/wyrmstooth.jpg" }})

The East Empire Company commissions the Dragonborn to slay a dragon that is interrupting trade routes throughout Skyrim. But is the dragon stirring up trouble with a particular reason or is there something more sinister behind it? An immersive new quest that takes the Dragonborn across Skyrim to the large island of Wyrmstooth. Battle across new landscapes and through new dungeons in this unofficial expansion. The task seems quite simple for the Dragonborn: slay a dragon that's stirring up trouble with shipping in the area. But things don't go that easily...

Wyrmstooth, sometimes referred to as The Elder Scrolls V: Wyrmstooth, is a DLC-sized expansion of The Elder Scrolls V: Skyrim that took place on the island of Wyrmstooth. It was created by Jonx0r and featured an entirely new main questline, side quests, new npcs and much more. The quest Dragon Hunt is initiated automatically after completing Dragon Rising if the Dragonborn's level is above 10.


### Summerset Isle ([Nexus](https://www.nexusmods.com/skyrim/mods/68406/))

Summerset Isle, a Skyrim expansion mod of DLC proportion, continues the story of the Arch-Mage of Winterhold when they are summoned to the land of the Altmer. Mystery and Heroic Fantasy with quest openness to explore, make choices, and mistakes. Your actions will have consequences. 

### Clockwork ([Nexus](https://www.nexusmods.com/skyrim/mods/77809/))

Clockwork is a quest mod involving a castle high in the Velothi Mountains, lost to the world for over a hundred years. Once found, you may call Clockwork Castle home... but if you want to leave, you'll need to discover how to escape.

Will start once you reach level 5.

![Clockwork 1]({{ "assets/clockwork1.jpg" }})

![Clockwork 2]({{ "assets/clockwork2.jpg" }})

### The Forgotten City ([Nexus](https://www.nexusmods.com/skyrim/mods/70219/))

The Forgotten City is the first mod in history to win a national Writers' Guild award with its script. It is a critically acclaimed expansion mod offering a unique 6 - 8 hour experience: a murder mystery investigation set in an ancient underground city. You'll need to solve it using your wits, and the ability to travel through time. It has a dark, non-linear story in which you'll interrogate suspects, explore the city and its many secrets, and navigate challenging moral dilemmas. It features multiple endings, an original orchestral score, and professionally voiced dialogue.

It’s designed for characters at Level 5 and above. However, the story responds to your character’s individual history, so a high level is recommended. If you’re Level 5 or above, you’ll be approached automatically by a courier when you enter any city. Alternatively, you can make your way to the "Forgotten Ruins" in the south west corner of Skyrim.

This mod is designed to be played solo. You'll understand why once you play the game.

![Forgotten City]({{ "assets/forgotten_city.jpg" }})

### Aethernautics - A Space Travel Mod ([Nexus](https://www.nexusmods.com/skyrim/mods/41754/))

Another dwemer-centric mod, the title to this is pretty self-explanatory. But to get the promised spaceship you’ll have to find out where the hell it is first. You might ask "how hard can it be to find a gargantuan spaceship?". Answer: surprisingly hard. If a quest wasn’t enough you’ll also be voyaging through a dungeon that comes with a brand new type of enemy, as well as wielding radically different weapons (which may or may not make a pew pew sound). If you can overlook the few issues the mod has, it’ll add a completely new dimension to Skyrim.

* Read the notes and pay attention! This quest is unmarked, and as such you're going to actually have to use that thing you call a brain! Shocking I know, and it might be difficult, but I believe in you.
* Be sure to save often, many times you'll find yourself in difficult or precarious positions, and it might help to reload earlier saves! 
* I recommend finding how to fix navigation before leaving the ship (the tonal pad platform) to go back to Tamriel. Other than that you're fine. If you do end up leaving the ship, you'll have to open the console and type 'player.coc _an_aethership_bridge' to get back. 
* Followers won't work in the dungeon or aboard the ship, the platforming sections are just too nuts for them. However they may still run around on the bridge. 

![Aethernautics]({{ "assets/aethernautics.jpg" }})

### Blackreach Railroad ([Nexus](https://www.nexusmods.com/skyrim/mods/63403))

Reactivate and restore your very own dwemer railroad, with new characters, items, and a fully voiced quest to do so. 

Deep in the bowels of the earth, the Dwemer constructed vast cities of gleaming bronze and chiseled stone. Their networks criss-crossed all of northern Tamriel, stretching from Hammerfell in the west all the way to Morrowind in the east. Now, rumors of a gleaming steam engine have emerged from the depths of Blackreach, capable of travelling at enormous speeds, and able to connect the furthest-flung outposts of the buried Dwemer civilization. Will you be able to unlock its mysteries?
Blackreach Railroad is a travel and quest mod for the steam-oriented player. It adds a locomotive, a train line, and a series of train stations around Blackreach and other Dwemer ruins. After a short quest to reactivate the train, the player may then freely ride the engine throughout Blackreach. 
In addition to this, the player may choose to ride on uncharted track, thereby unlocking new stations throughout Skyrim.
And for those wondering, you do indeed get a train hat.

![Blackreach]({{ "assets/blackreach.jpg" }})

### Moon and Star ([Nexus](https://www.nexusmods.com/skyrim/mods/52397))

A mysterious and powerful criminal has been spotted in Skyrim, his trail dogged by hunters from Morrowind. However, there may be more at stake than it seems, and this criminal may be familiar... 



### Honorable Mention: [Enderall](https://enderal.com/)

Enderal is a total conversion for TES V: Skyrim: a game modification that is set in its own world with its own landscape, lore and story. It offers an immersive open world, all for the player to explore, overhauled skill systems and gameplay mechanics and a dark, psychological storyline with believable characters.

Completely standalone game on the Skyrim engine.

![Enderall 1]({{ "assets/enderall1.jpg" }})

![Enderall 2]({{ "assets/enderall2.jpg" }})

![Enderall 3]({{ "assets/enderall3.jpg" }})



## Added Content

### [Amorous Adventures](https://www.nexusmods.com/skyrim/mods/70495)

Fully voiced dialogue and immersive romance type quests with vanilla Skyrim NPCs. A bit of adventure, romance and occasionally romantic comedy while exploring the vanilla NPC's backstories more.

![Amorous Adventures]({{ "assets/amorous0.jpg" }})

* The majority of Romance-able NPC's are female; there are two Males.
* A DLC sized Quest Mod.
* Fully voiced in the original NPC's own voices - over 3,500 fully voiced lines.
* Story driven, plot driven romantic encounters.
* Integrates seamlessly into a Skyrim play through - like the romance was there all along.
* Over thirty lovers to get - catch them all like Pokemon.

![Amorous Adventures]({{ "assets/amorous1.jpg" }})

### [Interesting NPCs](https://www.nexusmods.com/skyrim/mods/8429)

Over 250+ fully voiced NPCs, 25+ followers, 15+ marriage NPCs, and 50+ quests.

![Interesting NPCs]({{ "assets/npcs.jpg" }})

Interesting NPCs is a project to add color and life to Skyrim through three-dimensional characters. Each NPC is integrated into the world, with a backstory and an extensive dialogue tree to explore. Many of these characters are fully voiced by a talented team of over 80 voice actors. The dialogue choices allow you to role-play, providing humor and depth to each conversation. You can be a jerk or a jester, a white knight or an assassin, as the most important character is you.

Originally, the goal was to expand 8 followers to include commentary on every quest and location in the game, including bonus conversations during or after major questlines. Currently, these followers, whom I have dubbed "Super Followers" because they fly and wear capes, have approximately 1000+ lines each. It may be unrealistic to meet this goal, however, as actor interest in performing slave labor can ebb and flow, and even a dozen new lines can often take months to get recorded.

As the mod will likely continue to make future additions and improvements, it can still be classified as a WIP, but the existing content should see very little change.

### [Expanded Towns and Cities](https://www.nexusmods.com/skyrim/mods/13608) -- New Improved WiP

![ETaC]({{ "assets/etac.png" }})

While the new version is certainly in the spirit of its predecessor, it is NOT the same mod. So if you came here expecting identical towns, with identical people and identical layouts – Prepare yourself, because you’re about to be sorely disappointed lol. This was built from the ground up, taking into account everything I have learned in MY CONSIDERABLE EXPERIENCE AND/OR WISDOM (lol jokes, I clearly have neither of those things) to try and give you and me and everybody a much better mod overall.

I made a lot of mistakes with the old ETaC. Things that, given the opportunity (and a time machine), I would never had done in the first place. Mostly for my own sanity. As it turned out, I now HAVE that opportunity to build the mod I’d always wanted to build, so buckle up mod-friendos, we’re going to go on a little adventure together. Which brings me to…

List of changes!

As for specifics, I’ll be omitting the small "aesthetic" changes from these list (i.e. clutter, trees, wildlife, lanterns, blah blah) as identifying every single barrel I’ve moved and tree I’ve added would be a huge enormous pain in my arse. (Read: am lazy! HI!) Anyway. (Those things excluded) here's what you get...

#### Darkwater Crossing

- Added 6 new houses.
- Added 9 new NPCs.
- Added new custom dialogue for said NPCs
- Added new quest (misc): Riften Delivery.
- Added some vanilla options that were available in other towns, but not here for whatever reason...
- Like the gathering of wood and food for those of you who had fervently wished you'd been born into a life of farming and woodcutting.
- Added a general store.
- Added an inn/tavern for the sleeping and the consuming of the wines.
- Added a shrine and alchemy bench for the healing of your poor diseased self.
- Added a well for drinking water.
- Added exterior chimneys for houses because fireplaces.
- Added additional farms, farmers, fishermen, and hunters.
- Added additional woodcutters; And, NPCs will collect firewood for their houses daily.
- Gave vanilla homeless NPCs homes (Derkeethus, Sondas, Tormir and Hrefna.)
- Derkeethus Quest - People in Darkwater will now react to Derkeethus' absence/return.
- Derkeethus Quest - Added the ability for talking to the aforementioned people to start his quest.
- Derkeethus Quest - Derkeethus' house will reflect whether or not he's living in it.
- Changed Verner's house exterior mesh to match interior layout.
- Made extensive changes to town layouts, landscape, navmesh and NPC AI *


#### Riverwood

- Added 6 new houses.
- Added 9 new NPCs.
- Added new custom dialogue for said NPCs
- Added new quest (side): A Potion in Time.
- Added new quest (side): The Collector Pt. 1.
- Added new quest (side): The Collector Pt. 2.
- Added new quest (side): The Collector Pt. 3.
- Added new quest (side): The Collector Pt. 4.
- Added an alchemist store.
- Added a well for drinking water.
- Added exterior chimneys for houses because fireplaces.
- Added additional farms, farmers, fishermen, and hunters.
- Added additional woodcutters; And, NPCs will collect firewood for their houses daily.
- Gave vanilla homeless NPCs homes (Embry.)
- Made extensive changes to town layouts, landscape, navmesh and NPC AI (People now do a bunch of things beyond standing around and eating bread, om nom nom.)


### [Immersive Armors](https://www.nexusmods.com/skyrim/mods/19733)

![Immersive Armors]({{ "assets/im_armors.jpg" }})

Immersive Armors seeks to drastically enhance the variety of armors in the world of Skyrim in a lore friendly way. The goal of every set is to blend into the lore, balance, and feel of the game for the most immersive experience possible.

If you're seeking to enhance your Skyrim with a greater selection of armor sets without breaking from the natural feel of the game, this mod is for you. It adds many new armor sets that have been seamlessly integrated into the world. The items are balanced and spread across your gaming experience. You will be finding new, power appropriate armor sets starting from level one up to level fifty and beyond.

This mod will be constantly evolving to bring new life into your world. Currently this mod adds 55 new sets of armor to the game (over 60 including variants), tons of additional non-set helmet options, 396 new shields, and a large number of other items such as eye patches, capes, earrings, scarves and more! They are all craftable, upgradable, enchantable and are integrated into the game through leveled lists, quest rewards, placement in dungeons and/or onto specific people.
### [Immersive Weapons](https://www.nexusmods.com/skyrim/mods/27644)

![Immersive Weapons]({{ "assets/im_weapons.jpg" }})

Immersive Weapons seeks to drastically enhance the variety of weapons in the world of Skyrim in a lore friendly way. The goal of every addition is to blend into the lore, balance and feel of the game for the most immersive experience possible.

If you're seeking to enhance your Skyrim with a greater selection of weapons without breaking from natural feel of the game, this mod is for you. It adds many new custom weapons that have been seamlessly integrated into the world. The items are balanced and spread across your gaming experience. You will be finding new, power appropriate weapons starting from level one up to level fifty and beyond.

This mod will be constantly evolving to bring new life into your world. Currently this mod adds 224 new weapons and 6 new arrows to the game! They are craftable, upgradable, enchantable as appropriate and are integrated into the game through leveled lists, vendors, placement in dungeons and/or onto specific people.



### [Skyrim Immersive Creatures](https://www.nexusmods.com/skyrim/mods/24913)

![Immersive Creatures]({{ "assets/creatures.png" }})

Skyrim Immersive Creatures adds dozens of new creatures to enhance your gaming experience. This mod adds brand new creatures which fit in to the world of Skyrim, alongside variations of existing creatures, adding diversity and improving gameplay.

New creatures include new types of undead, dwemer automatons, warring goblin tribes - and many, many more!

Creatures have new special abilities, with strengths and weaknesses, and by default are more challenging than existing creatures in the game -
encouraging the player to use different strategies to deal with different creatures.  Difficulty can be adjusted in the MCM menu.

All creatures fit the world of Skyrim, but most creature types are can be enabled or disabled in the MCM menu
to suit your own taste.  For those who seek a purist lore-friendly experience, the 'purist' preset enables only
those creatures seen in earlier Elder Scrolls games, or with precedence in established written lore.


### [Bandolier - Bags and Pouches](https://www.nexusmods.com/skyrim/mods/16438)

Adds lots of pouches, bags and bandoliers to help easing your burden.

Have you ever left behind tons of useful stuff, simply because you could not carry anymore? How many dragon bones, dwemer scrap or other valuable things were abandoned? Too much to count. But now, there is a solution just for you! "Bandolier - Bags and Pouches" offers you a wide selection of various equipment items that should ease your burden! And it can be used with nearly every armor!

![Bandolier]({{ "assets/bandolier.jpg" }})

#### How to get

All items are crafted at the tanning rack. However, some items may require extra things that can be crafted at the forge.
Black Dyed Leather is crafted by combining Leather and Frostbite Venom.
Assorted Buckles are crafted at the forge from Steel Ingots.
To make a bandolier with a shoulderpad, you must first make a regular one, and then, combining it with an extra piece of leather at the tanning rack, you can create bandoliers with shoulderpads.
To make Dark Botherhood Items, you must first get the recipe from thier sanctuary.
You can upgrade your small bandoliers into the large ones. The recipe will only show if you have small bandolier in your inventory.


## Survial Mods

### [Campire - Complete Camping System](https://www.nexusmods.com/skyrim/mods/64798/)

Campfire is the most feature-rich stand-alone camping mod for The Elder Scrolls V: Skyrim, created from Frostfall's immersive and detailed camping system. 

![Campfire]({{ "assets/campfire.jpg" }})

**Build a campfire**, from a small, fragile spark to a roaring blaze, and use it to cook with a cooking pot (or just grill right over the flames), sit with your followers, and keep warm with mods like Frostfall.

Create and buy **camping equipment** using items in the world around you. Build tents, tanning racks, a portable enchanting kit, and more. Carry all of your extra gear in Campfire-added backpacks that dynamically display your bed roll, torch, cooking pot, axe, and water skin.

Earn new abilities using Campfire’s new **stand-alone skill system**. Search the nearby area for deadwood and branches. Use these to build hatchets, arrows, and other useful creations. **Stalk prey** using the **Instincts** power to see moving or noisy creatures nearby and easily find valuable tinder for your campfires.

See the official site for [how to build camp fires](http://skyrimsurvival.com/home/campfire/building-campfires/).


### Frostfall - Hypothermia Camping Survival ([Nexus](https://www.nexusmods.com/skyrim/mods/11163))

![Frostfall]({{ "assets/frostfall1.png" }})

Frostfall is a survival-style mod that adds cold weather survival gameplay elements to Skyrim. 

The three main components of Frostfall are **Hypothermia**, **Cold Water Survival**, and **Camping**.

The primary objective of this mod is to add a deep, immersive level of gameplay to Skyrim, while keeping tedium to a minimum. An immersion mod should never get in the way of having fun.

Frostfall uses a sophisticated system to track your location, weather, time of day, worn clothing, and more, to determine your current condition, in a seamless and immersive way. Combined with Campfire, it also features a large variety of craftable camping equipment, including craftable torches. It is highly customizable, and very compatible with most other mods, including Real Shelter, Climates of Tamriel, Wet and Cold, worn cloaks, food, timescale, lighting, werewolf, and vampire mods.

![Frostfall]({{ "assets/frostfall2.jpg" }})

See the [How to Play](https://skyrimsurvival.com/home/frostfall/how-to-play/) on the official site.

### Realistic Needs and Diseases ([Nexus](https://www.nexusmods.com/skyrim/mods/26228))

**Progressive Diseases:** Diseases are no longer easily ignored nonsense. Initial syndromes are still mild (as in vanilla), but without treatment, things get worse, much worse, over time. Find a cure as soon as possible! But if you can't afford a cure, you can always rest and hope for the best, there is a chance your body will slowly fight off the disease by itself if you get enough rest. By rest, I mean a clean bed and warm fire, you're likely to catch more diseases sleeping in a dirty bandit camp!
Disease Fatigue, if enabled, you will get tired more quickly, also can't sleep well when diseased. Please check for disease effects first if your character suffers sleep disorder.

**Check Needs:** When you start the mod, you will gain a “Check Needs” power, so you can bind it to a hotkey for quick checking. You can also enable debug mode to have it display needs points.
**Hotkeys:** If you have SKSE, this mod also adds hotkeys for Check Needs <default N> and Drink from Stream<default B>, the drink key doubles as “empty” key, if you want to empty a bottle or waterskin without drinking its content, highlight the bottle/wine in inventory, then hit B; the keys are configurable in MCM.

**Hunger** has 6 stages:
Gluttony - I have eaten too much. Speed -30%.
Satiated - Health, stamina regeneration +10%.
Peckish - Ready for a snack. (no bonus or negative effect)
Hungry - Health, stamina regeneration -30%.
Very Hungry - Health, stamina regeneration -60%. Sneak is 25% harder. Attack damage -25%.
Starving - Health, stamina regeneration -90%. Carry Weight -50. Attack Damage -50%.

**Thirst** has 5 stages:
Quenched - Magicka, stamina regeneration +10%.
Slightly Thirsty - Ready for a drink. (no bonus or negative effect)
Thirsty - Magicka, stamina regeneration -30%.
Very Thirsty - Magicka, stamina regeneration -60%. Time between shouts +25%. Spells 25% less effective.
Dehydrated - Magicka, stamina regeneration -90%. Time between shouts +50%. Spells 50% less effective.

Drinking ale/mead or wine will give you empty bottle, any vanilla "empty wine bottle" you pick up will also be converted into reusable ones.
You can refill empty bottles and waterskins in any river (stand in water then click the bottle), or you can ask for clean water in inns and taverns.
Drinking river water directly can cause diseases, boil before drinking. Spring water (currently only available from drinking fountains) is safe to drink.
Herbal Tea will give you +10 speech for 300 seconds, milk is also available if you don't mind being a milk drinker.
If you have installed Drinking Fountains of Skyrim, activate the fountain to refill all your empty bottles, or sneak-activate to drink directly from the fountains.

For cooking, you need to convert bottled water into plain “water” (flagon), I did this so I don't have to duplicate recipes for different types of water containers. Water conversion gives you empty bottles back. Many innkeepers now also stock everyday cooking ingredients, things like salt, garlic, honey, chicken eggs, elves ears, frost mirriam, etc.
Seawater: If you run out of salt, you can collect seawater using empty bottles along the coastline, boiling seawater gives you salt.

**Food Spoilage**, if enabled, this mod will keep track of food items in player inventory, you can configure how long it takes for each type of food to spoil, you can also collect (25% chance) molds and rotten flesh from those spoiled junk, and use them in alchemy.

**Inebriation** has 5 stages: With visual and stumbling effects.
Sober - I'm sober. (no bonus or negative effect)
Dizzy - Good stuff, just a bit dizzy. Unarmed Damage +5, Damage Resist +10.
Drunk - My head is spinning. Unarmed Damage +10, Speech -15, Magicka regeneration -15, Spells 50% less effective, duration -50%.
Wasted - I'm completely wasted...Magicka and Stamina regeneration -25, Spells 85% less effective, duration -85%.
Blackout - Knocks you out right on the spot, only to wake up 4 hours later head spinning.

**Sleep** has 5 stages:
Well/Rested - Same as in vanilla
-Werewolf will get a "Restless Beast" stage instead, just to keep it lore friendly.
Slightly tired - Ready for a nap. (no bonus or negative effect)
Tired - Carry Weight -30, Skills improve 25% slower.
Very tired - Carry Weight -60. Skills improve 50% slower. Speed -20%.
Exhausted - Carry Weight -90, Skills improve 75% slower. Speed -30%.

You must sleep for enough hours to get fully rested. There is no need to sleep consecutive hours though, if you wake up still tired, you can simply go back and take another nap. The "dirty bedroll" disease chance is actually location based, not which bedroll you use. The name itself is a bit misleading. Because if the place is filthy you bedroll is doomed to get dirty once you throw it down on the floor. ;) Avoid locations like dungeons, bandit camps, animal dens etc., pick a clean spot and you will be fine.

### [Hunterborn](https://www.nexusmods.com/skyrim/mods/33201)

Changes animal looting into a process of dressing, skinning, harvesting, and butchering. Adds hunting knives, many new alchemy ingredients, animal meat, recipes, and foraging. 

#### Features:

The most significant change Hunterborn makes to the game is taking away the loot window from most wildlife. Instead, you must first choose whether to dress it (clean the carcass and prepare it for processing) where it lies, or pick up the carcass and take it back to your camp / lair / nearest vendor.

![Hunterborn]({{ "assets/hunterborn_inline1.png" }})

When you drop the "carcass" from your inventory, the original animal's body will appear at your feet, where again you have the choice to dress it, or pick it back up. After dressing a carcass, you're able to skin it, harvest it for ingredients like antlers, eyes, teeth, even hearts, and also butcher it for meat.

![Hunterborn]({{ "assets/hunterborn_inline2.png" }})

**Field Dressing** - the first step in rendering down an animal for its goods is to "dress", or "clean", the carcass. This is a simple process that eliminates the most common causes of meat spoilage, and usually involves exsanguination.

**Skinning** - after dressing the carcass, usually the next step is to skin it - though you can choose these actions in any order, and skip any of them as well.

**Harvesting** - Besides the pelt and the meat of the carcass, all animals will have valuable materials that you can harvest from them. Most of these materials are alchemy ingredients, and Hunterborn adds many new custom ingredients for you to harvest and use in the creation of poisons and potions - and the effects on these custom ingredients have been designed to be specifically useful for hunter-style play.

**Butchering** - Hunterborn adds new custom meat types for all of the hunting game in Skyrim - and even for monstrous creatures like chaurus and dragons, if you have Monster Hunter enabled. Elk will have a distinct kind of venison, foxes and wolves have their own meat, and even mudcrabs and slaughterfish can now be a source of sustenance.

![Hunterborn]({{ "assets/hunterborn_inline3.png" }})

When first started, Hunterborn assumes that you are a novice hunter with no experience, so the first time you field dress a carcass, it will take a lot of time, especially if it's a larger carcass. You'll get faster at dressing, skinning and harvesting every single time you complete the action and you will get better (higher quality, and more) results as your skinning and harvesting level increases. Turning on the hunting knife requirement in the MCM will also make a difference in your yields.

There are a plethora of other features for hunter-style play: Hunting knives that can give bonuses to skinning and harvesting; 47 new alchemy ingredients that you can harvest from animals (and monsters), some with unique effects; new meat types and cooking recipes for Skyrim's wildlife; Scrimshaw crafting for animal bones; storage-where-you-need-it in the form of hunter's caches; and even foraging with location-specific results. A detailed readme and an additional spoilers readme are included.

![Hunterborn]({{ "assets/hunterborn_inline4.png" }})

**Scrimshaw** is a crafting system unique to Hunterborn intended to provide multiple uses for the bones you harvest from animal (or monster) carcasses. Use it to craft bone weapons, hunting knives, arrows, armor and jewellery. Polish animal eyes for new ingredients. Engrave Bones to cure diseases.  Create cache markers to store your stuff. New recipes are unlocked as you increase your Harvest skill.

**Forage** - search the surrounding terrain for useful materials which can include edible plants, local alchemy ingredients, animal bones, and some miscellaneous items such as firewood. The materials you find will be based on the environment - herbs and edibles are plentiful in the south, while north and at higher elevations it will be difficult to find much of anything. Level Foraging to unlock Bounty and Botany perks and improve Sense Direction.

**Sense Direction** - immersively discover the direction you are facing. Accuracy improves as you level your Foraging skill.

**Botany** allows the player to learn extra effects from ingredients by consuming them. This works identically to the Alchemy perk Experimenter. Improve Forage to level 6 to unlock the first rank.

**Strange Brew** can be brewed with a wolf eye and nord mead. When drunk, you gain access to a new series of recipes that only remain available while you're drunk. Some are at the cookpot, some are in Scrimshaw. Additionally, there is a hidden levelling system for Strange Brew. Get brewed up, make one of the hidden recipes, and after you sober you'll level up. Then the next time you have a quaff, you have access to more recipes. I won't give any spoilers suffice to say that you can craft poisons, potions, meals, animal lures, bone idols and jewellery.

**Animal Lures** are crafted while drunk on Strange Brew. Smelly Meat lures a nearby aggressive wolf. Tasty Carrot and Chicken, however, will tame Rabbits, and Foxes (including snow foxes) making them permanent animal followers.

**Primitive Cooking** is a new crafting skill allowing the player to sear meat over an open flame. It needs no furniture, although you must be standing close to a heat source, and it recognises all fire's in the game.

&&Monster Hunter** is an optional module in Hunterborn that extends the same basic idea - you must take time and work on a slain monster's carcass to acquire material components, including meat - to the basic monster types. Monster Hunter is off by default, you can enable it in the MCM as well as toggle on or off each monster type and set some options.

**Blood and Venom** - with Monster Hunter enabled, you'll be able to extract either venom or blood from monsters. Chaurus and (Frostbite) spiders yield venom; dragons, trolls and werewolves yield blood, though dragons do not yield blood unless the "Corporeal dragons" option is checked, in the MCM.

**Potions and Poisons** - Troll and werewolf blood can be used to create either a beneficial potion or a poison. The strength of the result is determined by your harvesting level. Dragon's blood is different. Rather than using ingredients and your harvesting level, you gain access to different recipes by learning new dragon shouts.

**Rites of Hircine** - What does this Strange Claw do? Hunterborn includes a set of three hidden quests that unlock unique abilities. These quests don't show up in your journal and they don't give you many hints on how to proceed. Unless you happened upon a hunter NPC selling a certain book, it can be very challenging to solve the three quests on your own.

**MCM Settings** - Some things you can do with the MCM settings; toggle off the time component to process animals, change the default action when you click on an animal, alter meat and ingredient yields, change pelt values, remove the manual loot option, enable the hunting knife requirement, turn off animations, enable the Botany Perk, set hotkeys, check and reset your stats, enable Monster Hunter, use other mods custom pelts and meats for greater compatibility, enable the taxonomy power to recognise new animals from other mods, toggle on the Hunting in Skyrim skinned animal textures, turn on Fur Plate crafting recipes, plus lots more...


#### [Hunterborn - Scrimshaw Expanded](https://www.nexusmods.com/skyrim/mods/71573/)

Adds hundreds of new recipes and items to Hunterborn's Scrimshaw feature.

![Scrimshaw]({{ "assets/scrimshaw_expanded.jpg" }})

* Do you ever wonder what to do with those extra teeth, claws, and bones that you keep finding?
* Do you need weapons and armor, but hate mining and smelting ingots just to stay versatile?
* Do you want to pimp out your Dragonborn with antlers, bones, skulls, and other natural-looking swag?
* Do you want to make cooler toys to give to your children?
* Do you want to make a magic staff, but you don't want to travel all the way to Solstheim to do it?

## Immersion

### [Jobs of Skyrim](https://www.nexusmods.com/skyrim/mods/81845/)

Ever wanted to earn money without entering dungeons or doing dangerous tasks for potentially shady people? Yeah? Well, this is the mod for you, my friend! This mod adds 22 jobs for you to do around Skyrim, with shadiness and danger being optional.

#### Jobs and NPCs

* Alchemist
* Blacksmith
* Bounty hunter
* Brewer
* Cook
* Courier
* Delivery man
* Dragon hunter
* Enchanter
* Guard
* Hall of The Dead worker
* Hired Assassin
* Hired Thief
* Jewelcrafter
* Merchant assistant
* Necromancer
* Poacher
* Priest
* Tailor
* Tax collector
* Waitress/bartender

#### Notes

* Some quest don't have a marker for completion. This means you can turn it to any eligible quest giver - if you took the quest in Riverwood, you can complete it at the same NPC in Rorikstead. Quests that have to be turned to a specific NPC will always have a marker on them.
* For alchemy and enchanting, the quest is being done by having you do the potion/enchantment through a custom craft station ie: a satchel for alchemy and a *Daedric hand* for enchanting. Make sure to look at the objective markers properly since they will lead you to the good crafting station to make the job work properly.
* Some NPCs will not have any dialogue unless you bring them something. Try bringing a pelt collector wolf or bear pelts, a fisherman fish, etc.

### [Become a Bard](https://www.nexusmods.com/skyrim/mods/65636/)

Become a Bard lets you play all of the instrumental and voiced songs included with the game or choose from dozens of new songs. It also adds new radiant quests to expand the options for a bard in Skyrim.

### [Cloaks of Skyrim](https://www.nexusmods.com/skyrim/mods/12092/)

Adds almost 100 new styles of cloak to the world of Skyrim, via crafting, levelled-lists, and static loot. They are lore-friendly and enchantable, and more are being added with every update. The aim of this mod is to provide some variety and flavour to the world, as well as the chance to wear a stylish cloak and quite literally keep the cold out. 

### [Wet and Cold](https://www.nexusmods.com/skyrim/mods/27563/)

This mod adds weather-dependent visual effects and NPC AI enhancements.

### [Immersive Patrols](https://www.nexusmods.com/skyrim/mods/12977)

Immersive Patrols adds a reasonable amount of Stormcloak, Thalmor, Imperial and Dawnguard patrols to Skyrim and Skaal, Reaver and Riekling patrols to Solstheim. These patrols often cross paths with each other or the player creating new encounters or unexpected aid. It also includes small Civil War fort battles and some non-Khajiit Caravans. 

### [Guard Dialogue Overhaul](https://www.nexusmods.com/skyrim/mods/23390)

More Dialogue, More Realism, More Respect. This Mod alters the Guard Dialogue, changing it from something that breaks your immersion in the world of Skyrim to something that actually increases it. All in the original voices and Fully compliant with Skyrim Lore. 

### [Perseids Inns and Taverns - Realistic Room Rental Basic](https://www.nexusmods.com/skyrim/mods/25029)

Ever wonder why in Skyrim all hotel rooms cost the same? For 10gp you can't even buy a cheese wedge, but you can get a luxury suite in the Winking Skeever. This simply makes no sense. To make room rental system more realistic, I have assigned local prices to all inns.

### [Dynamic Things - Woodpiles and Barrels and More Oh My](https://www.nexusmods.com/skyrim/mods/32448)

Turn useless "statics" in Skyrim into something useful!
Turn woodpiles, barrels, crates and hay bales into containers with appropriate contents.
Drink or bottle mead, water, skooma or blood from mead barrels with spigots.
Gain experience from training dummies and archery targets.
And much more...

## Overhauls

### [Ordinator](https://www.nexusmods.com/skyrim/mods/68425)

Ordinator overhauls the perk trees of Skyrim, replacing them with ~400 new perks to improve the depth and fun of building and playing your character. It accomplishes this in a lightweight, compatible and clean fashion. 

#### Features:

* All perk trees overhauled: over 400 new perks.
* Enables many new viable character builds.
* Compatible with almost everything, easily made compatible with the rest.
* Lightweight scripts, no save bloat.

#### Thief Skills Rebalance

To go with the new thief perks, there is an optional file available on the downloads page that modifies many game settings related to sneaking, lockpicking and pickpocketing to improve the thief experience: the effectiveness of sneaking now depends more on light level and movement and less on skill level, the pickpocket cap is 95%, and more. A full list of changes is in the Readme.

### [Immersive Citizens - AI Overhaul](https://www.nexusmods.com/skyrim/mods/65013)

![Immersive Citizen]({{ "assets/im_cit.jpg" }})

Immersive Citizens - AI Overhaul is a mod whose purpose is to increase player in-game immersion by greatly improving Artificial Intelligence (AI) of friendly NPCs (citizens) in order to make them act and react like true humans in relation to their environment or to an aggressor. Specifically, combat AI is revamped, with citizens using a new “Survival Instinct” feature that gives them the ability to evaluate danger from an aggressor and respond to it more realistically while under attack. Their non-combat behavior has also been expanded, with new and realistic behaviors including improved schedules, occasional travel, and better use and response to the world around them. Whether fighting or living their lives, NPCs will respond to things like the weather, or an individual’s profession and relationships. The end result should add considerable depth to the world around you. 

### [Apocalypse - Magic of Skyrim](https://www.nexusmods.com/skyrim/mods/16225/)

Apocalypse is the most popular Skyrim spell pack, adding 155 new spells that are unique, balanced, lore friendly, use high quality custom visuals and blend seamlessly into the vanilla magic system. Also includes scrolls and staves for the new spells. 

### UFO - Ultimate Follower Overhaul ([Nexus](https://www.nexusmods.com/skyrim/mods/14037))

UFO is a set of tools and enhancements to the followers system that will allow for better control and usability of followers.

### [Immersive College of Winterhold](https://www.nexusmods.com/skyrim/mods/36849)

An overhaul of the buildings and inhabitants of the College of Winterhold.

### [Enhanced Skyrim Factions - The Companions Guild](https://www.nexusmods.com/skyrim/mods/22650)

Tired of being forced to be something you don't want to be? Think it's silly that a low-level mage with minimal weapon and armor skills can become leader of this famous band after only two weeks? No longer. Now you must truly earn your place among your Shield-Brothers and Sisters. 


## Followers

### [INIGO](https://www.nexusmods.com/skyrim/mods/40960)

Inigo is a fully voiced khajiit adventuring companion with over seven thousands lines of unique dialogue - much of it about you. 

* He's essential. 
* He’ll level alongside you. 
* He’ll level alongside you. 
* He’ll avoid most traps. 
* If you’re sneaking he won’t chatter. 
* If you talk to him while sneaking he’ll whisper. 
* He can run out of arrows. 
* He’s highly skilled in archery, one-handed, and sneak. 
* He has unique, random combat dialogue for most enemies. 
* Your morality is his morality. 
* He tells stories, sings, and is influenced by your time together.

#### Where is he?

You will find Inigo in Riften Jail. He's next to Sibbi Black-Briar in the first cell on your left. Keep an eye out for notes referring to Inigo on your travels. If your
character needs a reason to visit the jail they may give you the excuse you require. Inigo is technically not a prisoner. You don't have to pick the lock to his
cell. Read the letter on the table on your way in to learn more.

![Inigo 1]({{ "assets/inigo1.jpg" }})

![Inigo 2]({{ "assets/inigo2.jpg" }})

### Arissa - The Wandering Rogue ([Nexus](https://www.nexusmods.com/skyrim/mods/53754/))

Arissa is an Imperial with an independent, rebellious spirit and a gift for making expensive things disappear. She is a Skyrim companion character with over 600 lines of dialogue, professionally recorded and beautifully voiced by the talented Nikkita. 


## Player Homes

### Castle Gonduin ([Nexus](https://www.nexusmods.com/skyrim/mods/63801/))

This mod adds a castle to the mountains southwest near Riverwood that can be used as player home. The castle is abandoned due to a curse that lays on it. After completing a quest and lifting this curse you can rebuild it part by part and make it a comfortable home. To start the quest just wait for the courier.

### [Hunter's Cabin of Riverwood](https://www.nexusmods.com/skyrim/mods/15878/)

The Hunter's Cabin of Riverwood is a lore friendly, free player home that's available to all players, almost as soon as you start the game. The perfect mod for those who want a beautiful, simple yet functional house from the very beginning, whithout feeling that you're ruining, or going against the lore. 

![Cabin]({{ "assets/huntercabin.jpg" }})


## Graphic Enhancements

### [Static Mesh Improvement Mod](https://www.nexusmods.com/skyrim/mods/8655)

A massive project to greatly improve the appearance of countless static 3D models in Skyrim. Basically, this is my attempt to make the Skyrim architecture, clutter, furniture, and landscaping much nicer. 

### [Climates Of Tamriel - Weather - Lighting - Audio](https://www.nexusmods.com/skyrim/mods/17802)

The #1 Weather & Lighting overhaul for Skyrim. 

### [Realistic Water Two](https://www.nexusmods.com/skyrim/mods/41076/)

New water surface textures were carefully crafted in an attempt to imitate the fluid motion of water. Lake, pond, river and ocean water are now visually and aurally distinct from each other. Various water effects have also been modified. 

## Miscellaneous

### [Alternate Start - Live Another Life](https://www.nexusmods.com/skyrim/mods/9557/)

Misery and despair have haunted you all your life. Just when you thought it couldn't get any worse, you've been arrested and thrown in the dungeons. Your trial was supposed to be weeks ago, only nobody has come for you. It's been so long since seeing daylight you no longer know what month it is anymore. The dungeon is cold, damp, and worse still, you're running out of food. At least you have that leak in the wall for water. It seems as though you've been forgotten, soon to rot away and die. The eight have forsaken you! Or... have they?

Live Another Life provides an alternative means to start the game for those who do not wish to go through the lengthy intro sequence at Helgen. You will be given the opportunity to choose your race and then choose a new life for your character to lead. A wide variety of choices will be available. What you choose will have a lasting impact, so choose carefully or the gods may forsake you again!

### [Convenient Horses](https://www.nexusmods.com/skyrim/mods/14950)

Horses for followers with scripted follower AI behavior and advanced features, mounted combat for followers, horse armor, mounted combat horse charge, dynamic faction relations, cinematic horse call, horse whistle, mounted conversations and herb gathering, horse inventory, fast dismount and much more.

### [Wearable Lanterns](https://www.nexusmods.com/skyrim/mods/17416)

Adds a craftable, wearable Travel Lantern that can be hooked onto your belt or held in your hand! Torchbug Lanterns, follower support, and more! 



### Sounds of Skyrim

Sounds of Skyrim is an audio project split in three parts that, in the end, adds around 460+ sound effects to the game world. It affects almost every cell and region in the game. My main goal is to not have one environment that is soundless to enhance immersion. The sounds are affected by the time of day, weather and location. So when walking in the city streets during the day, you'll hear the echo of doors opening and closing, children playing and workers sawing things. While at night, cats fight for territory and dogs bark.

#### [The Dungeons](https://www.nexusmods.com/skyrim/mods/8601)

The Dungeons includes 115+ sound effects to add life to the dungeons. The enemy type that inhabits a dungeon will make itself heard. At dungeons’ entrances, you also hear the exterior weather. If there’s a thunderstorm raging outside, you’ll hear the thunder rumbling in the fort or cave

There's a standard sound set for each type of dungeon: Cave, Dwemer Ruins, Forts and Sewers. These sounds play in these dungeons regardless of who or what lives in it. To spice it up, dungeons inhabited by Falmer or the Undead have their own sound sets in addition to the standard ones. Undead dungeons have zombies and wraiths screaming or moaning. In Falmer dungeons, you'll hear their occasional screams; a way of communicating with each other since they are nearly blind.


#### [The Wilds](https://www.nexusmods.com/skyrim/mods/10886)

The Wilds focuses on adding sound effects in Skyrim's wilderness. With the 115 new sound effects, the purpose of The Wilds is to audibly enlarge Skyrim's fauna with the addition of various animals, birds and insects that play according to weather, time of day and region. In the forests, for example, you'll hear the sounds of foxes, deers and ravens. At night, you'll hear wolves howling from far away or common loons calling others. In The Reach, you might hear the occasional mountain goat or hawk.

In addition to animals, The Wilds also adds rain-impact sounds on tents and wooden structures, sounds of rustling bushes to simulate the movements of small mammals that scatter away as you approach and sounds of calm water waves hitting wood on tree logs or structures that are partially submerged in water.

#### [Civilization](https://www.nexusmods.com/skyrim/mods/20193)

Civilization focuses on adding 223 new sound effects to cities, villages, farms and remote locations where civilization is present. In cities, you can hear the sound of a hammer being used by a villager repairing something, children playing, dogs barking or cats fighting. As you get closer to a tavern doorstep, the sounds of patron chatting from behind the door are played. When you get in, some patron may laugh out loud, yell or break something.

On farms or near them, you can hear the sounds of their animals, if they have any. Most of those sound effects can be heard inside buildings too, linking the interior cells with the exterior. If I take the farm example, you may hear the hen if you sit close to the window. If it’s raining, you’ll hear the rain hitting the windows; if there’s a snowstorm, you’ll hear the cold wind whistling by the windows.


### And more...
